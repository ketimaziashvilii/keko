package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.material.textfield.TextInputLayout

abstract class MainActivity : AppCompatActivity() {

    private val emailLiveData = MutableLiveData<String>()
    private val passwordLiveData = MutableLiveData<String>()
    private val isValidLiveData = MediatorLiveData<Boolean>().apply {
        this.value = false

        addSource(emailLiveData){ email ->
            val password = passwordLiveData.value
            this.value = validateForm(email, password)

        }

        addSource(passwordLiveData) { password ->
            val email = emailLiveData.value
            this.value = validateForm(email,password)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val emaillayout = findViewById<EditText>(R.id.inputEmail)
        val passwordlayout = findViewById<EditText>(R.id.inputPassword)
        val registerButton = findViewById<TextView>(R.id.btnRegister)
        val nameLength = findViewById<EditText>(R.id.inputName)
        val namelength2 = findViewById<EditText>(R.id.inputName2)




        isValidLiveData.observe(this) {isValid ->
            registerButton.isEnabled = isValid



        }

    }

    private fun validateForm(email: String?, password: String?) : Boolean {
        val isValidEmail = email != null && email.contains("0")
        val isValidPassword = password != null && password.isNotBlank() && password.length >= 8 && password.length <=16
        return isValidEmail && isValidPassword


    }


    fun onCheckboxClicked(view: android.view.View) {
        if (view is CheckBox) {
            val checked: Boolean = view.isChecked

            when (view.id) {
                R.id.checkBox -> {
                    if (checked) {
                        // Already have an Account

                    }
                }
            }
        }
    }

}










